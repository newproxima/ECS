/**
 * @file ecs.hpp
 * @author newproxima (gitlab.com/newproxima)
 * @brief ECS implementation file.
 * 
 * @copyright Copyright (c) 2021
 * @par License
 * @parblock 
 * The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * @endparblock
 */

#ifndef DEF_ECS_MANAGER
#define DEF_ECS_MANAGER

#include <cassert>
#include <memory>

#include <typeinfo>
#include <typeindex>
#include <bitset>

#include <array>
#include <queue>
#include <unordered_map>
#include <set>

// default ecs to optimise for memory (cf. documentation for more info)
// explicitly specify either of this directives
// before including the file to set a specific optimisation
#ifndef ECS_SPEED_OPTIMISATION 
#ifndef ECS_MEMORY_OPTIMISATION
#define ECS_MEMORY_OPTIMISATION
#endif
#endif

#ifndef ECS_MAX_ENTITY_COUNT
#define ECS_MAX_ENTITY_COUNT UINT16_MAX
#endif

namespace ECS
{
	/** @brief Maximum number of entities instanciated at any one time. */
	const uint16_t MAX_ENTITY_COUNT = ECS_MAX_ENTITY_COUNT; 
	/** @brief Maximum number of component types which can be registered. */
	const uint MAX_COMPONENT_TYPES = 32;
	/** 
	 * @brief Default number of components of any certain type. Can be modified on a per-pool basis. 
	 * @see Manager::SetMaxComponentCount(1)
	*/
	const uint COMPONENTS_POOL_DEFAULT_SIZE = ECS_MAX_ENTITY_COUNT;

	typedef uint16_t Entity; // used for representing entities
	typedef uint C_TYPE_ID; // used for indexing component types in the registry
	typedef uint C_ID; // used for indexing components in pools
	typedef std::bitset<MAX_COMPONENT_TYPES> Composition;	// used to represent which components are held by entities

/** @brief Classes to be used as "systems" in the ECS should inherit from this class. */
class System {
	friend class Manager;
	
	public:
		System() {}

	protected:
		/** 
		 * @brief A set of all the Entity-s which fit the systems composition requirements.
		 */
		std::set<Entity> composed_entities {};
};

class Manager {
	public:
		static Manager& getInstance() {
			static Manager _instance;

			return _instance;
		}

		Manager(const Manager& arg) = delete;
		Manager(const Manager&& arg) = delete;
		Manager& operator=(const Manager& arg) = delete;
		Manager& operator=(const Manager&& arg) = delete;

		// =============== ENTITIES ===============
		Entity CreateEntity()  {
#ifdef ECS_SPEED_OPTIMISATION
				assert(!available_entities_id.empty() && "cannot create entity ! (no available id)");

				Entity new_entity = available_entities_id.front();
				available_entities_id.pop();
#else //aka #elif defined(ECS_MEMORY_OPTIMISATION)
				assert(!(discarded_entities_id.empty() && entities_id_counter >= MAX_ENTITY_COUNT) && "cannot create entity ! (no available id)");

				Entity new_entity;
				if(discarded_entities_id.empty()) {
					new_entity = entities_id_counter++;
				} else {
					new_entity = discarded_entities_id.front();
					discarded_entities_id.pop();
				}
#endif

			return new_entity;
		}

		void DestroyEntity(Entity entity) {
			assert(entity < MAX_ENTITY_COUNT && "cannot destroy entity ! (out of range)");

			for (auto& pool : component_pools)
			{
				pool->OnEntityDestroyed(entity);
			}
			
			OnEntityDestroyed(entity); // (Systems)

			entities_compositions[entity].reset();

#ifdef ECS_SPEED_OPTIMISATION
				available_entities_id.push(entity);
#else //aka #elif defined(ECS_MEMORY_OPTIMISATION)
				discarded_entities_id.push(entity);
#endif
		}
		
		// ========================================


		// ============== COMPONENTS ==============
		
		template<class C>
		C_TYPE_ID RegisterComponent() {
			auto type_index = std::type_index(typeid(C));
			assert(component_composition_id_map.find(type_index) == component_composition_id_map.end() && "component type is already registered !");

			C_TYPE_ID new_c_type_id = component_composition_id_counter++;
			component_composition_id_map.insert({type_index, new_c_type_id});

			auto new_pool = new ComponentPool<C>();
			component_pools.push_back(std::shared_ptr<ComponentPool<C>>(new_pool));

			return new_c_type_id;
		}

		/**
		 * @brief Attaches a new component to the specified entity.
		 * @param entity The Entity to which attach the component.
		 * @param args Arguments to be passed on to the component constructor.
		 */
		template<class C, class... Args> 
		void AttachComponent(Entity entity, Args... args) { AttachComponent<C>(entity, C(args...)); }

		template<class C> 
		void DetachComponent(Entity entity) {
			C_TYPE_ID composition_index = GetComponentID<C>();

			GetCastedComponentPool<C>()->RemoveComponent(entity);

			entities_compositions[entity].reset(composition_index);

			OnEntityCompositionChanged(entity);
		}

		template<class C> 
		bool hasComponent(Entity entity) {
			C_TYPE_ID composition_index = GetComponentID<C>();

			return entities_compositions[entity].test(composition_index);
		}

		template<class C> 
		C* GetComponent(Entity entity) {
			assert(hasComponent<C>(entity) && "entity does not have this component");

			return GetCastedComponentPool<C>()->GetComponent(entity);
		}

		/**
		 * @brief Sets the new maximum size of the provided component pool.
		 *
		 * @param size The new size of the pool. Will be clamped up to the amount of components already in the pool.
		 */
		template<class C>
		void SetMaxComponentCount(const size_t size) {
			auto pool = GetCastedComponentPool<C>();
			pool->ResizePool(size);
		}

		// ========================================


		// =============== SYSTEMS ================

		template<class C>
		void AddToComposition(Composition& composition) {
			auto component_type_index = GetComponentID<C>();
			composition.set(component_type_index, true);
		}

		template<class C>
		void RemoveFromComposition(Composition& composition) {
			auto component_type_index = GetComponentID<C>();
			composition.set(component_type_index, false);
		}

		template<class S>
		std::shared_ptr<S> RegisterSystem(Composition composition) {
			auto system_type_index = std::type_index(typeid(S));
			assert(systems.find(system_type_index) == systems.end() && "system is already registered");

			auto system = new S();
			auto system_ptr = std::shared_ptr<S>(system);
			systems[system_type_index] = system_ptr;

			SetSystemComposition<S>(composition);

			// systems[system_type_index].reset();

			return system_ptr;
		}

		template<class S>
		std::shared_ptr<System> GetSystem() {
			auto system_type_index = std::type_index(typeid(S));
			assert(systems.find(system_type_index) != systems.end() && "system is not registered");

			return systems[system_type_index];
		}


		// ========================================


	private:
		Manager() {
#ifdef ECS_SPEED_OPTIMISATION
			// Populate the queue of available ids;
			for (Entity id = 0; id < MAX_ENTITY_COUNT; id++)
			{
				available_entities_id.push(id);
			}
#endif
		}

		// =============== ENTITIES ===============
		
#ifdef ECS_SPEED_OPTIMISATION
			std::queue<Entity> available_entities_id {};
#else //aka #elif defined(ECS_MEMORY_OPTIMISATION)
			std::queue<Entity> discarded_entities_id {};
			Entity entities_id_counter = 0;
#endif

		std::array<Composition, MAX_ENTITY_COUNT> entities_compositions {};
		
		// ========================================


		// ============== COMPONENTS ==============
		
		class IComponentPool {
			public:
				virtual ~IComponentPool() = default;
				virtual void OnEntityDestroyed(Entity entity) = 0;
		};

		template<class C>
		class ComponentPool : public IComponentPool {
			public:
				ComponentPool() {}
				ComponentPool(const ComponentPool& arg) = delete;
				ComponentPool(const ComponentPool&& arg) = delete;
				ComponentPool& operator=(const ComponentPool& arg) = delete;
				ComponentPool& operator=(const ComponentPool&& arg) = delete;

				void InsertComponent(Entity entity, C component) {
					assert(entity_to_index.find(entity) == entity_to_index.end() && "entity already has this component");
					assert(id_counter < pool.size() && "too many components of this type are already stored !");

					size_t new_index = id_counter++;

					pool[new_index] = component;

					entity_to_index[entity] = new_index;
					index_to_entity[new_index] = entity;
				}

				void RemoveComponent(Entity entity) {
					assert(entity_to_index.find(entity) != entity_to_index.end() && "entity does not have this component");

					C_ID removed_entity_index = entity_to_index[entity];
					C_ID last_entity_index = --id_counter;

					pool[removed_entity_index] = pool[last_entity_index];

					Entity last_index_entity = index_to_entity[last_entity_index];
					entity_to_index[last_index_entity] = removed_entity_index;
					index_to_entity[removed_entity_index] = last_index_entity;

					entity_to_index.erase(entity);
					index_to_entity.erase(last_entity_index);
				}

				C* GetComponent(Entity entity) {
					assert(entity_to_index.find(entity) != entity_to_index.end() && "entity does not have this component");

					C_ID index = entity_to_index[entity];
					return &pool[index];
				}

				void OnEntityDestroyed(Entity entity) override {
					if(entity_to_index.find(entity) != entity_to_index.end()) {
						RemoveComponent(entity);
					}
				}

				void ResizePool(const size_t new_size) {
					if(new_size <= id_counter)
						new_size = id_counter + 1;

					pool.resize(new_size);
				}

				C_ID id_counter = 0;
				std::vector<C> pool { COMPONENTS_POOL_DEFAULT_SIZE };
			
				std::unordered_map<Entity, C_ID> entity_to_index {};
				std::unordered_map<C_ID, Entity> index_to_entity {};
		};

		template<class C>
		C_TYPE_ID GetComponentID() {
			auto type_index = std::type_index(typeid(C));

			if(component_composition_id_map.find(type_index) == component_composition_id_map.end()) {
				RegisterComponent<C>();
			}

			return component_composition_id_map[type_index];
		}

		template<class C>
		void AttachComponent(Entity entity, C component) {
			C_TYPE_ID composition_index = GetComponentID<C>();

			std::shared_ptr<ComponentPool<C>> ptr = GetCastedComponentPool<C>();
			ptr->InsertComponent(entity, component);

			entities_compositions[entity].set(composition_index);

			OnEntityCompositionChanged(entity);
		}

		template<class C>
		std::shared_ptr<ComponentPool<C>> GetCastedComponentPool() {
			auto component_pool_index = GetComponentID<C>();

			return std::static_pointer_cast<Manager::ComponentPool<C>>(component_pools[component_pool_index]);
		}

		C_TYPE_ID component_composition_id_counter = 0;
		std::unordered_map<std::type_index, C_TYPE_ID> component_composition_id_map {};
		std::vector<std::shared_ptr<IComponentPool>> component_pools {};
		
		// ========================================


		// =============== SYSTEMS ================

		template<class S>
		void SetSystemComposition(Composition composition) {
			auto system_type_index = std::type_index(typeid(S));
			auto system = systems[system_type_index];

			system->composed_entities.clear();
			
			systems_compositions[system_type_index] = composition;
		}

		void OnEntityCompositionChanged(Entity entity) {
			for (auto const& pair : systems) {
				auto const& system_type_index = pair.first;
				auto const& system = pair.second;

				auto const& system_composition = systems_compositions[system_type_index];
				auto const& entity_composition = entities_compositions[entity];

				if((system_composition & entity_composition) == system_composition) {
					system->composed_entities.insert(entity);
				} else {
					system->composed_entities.erase(entity);
				}
			}
		}

		void OnEntityDestroyed(Entity entity) {
			for (auto const& pair : systems) {
				auto const& system = pair.second;
				system->composed_entities.erase(entity);
				// attempting to erase an element from a set within which there is no such element simply does nothing
			}
		}

		std::unordered_map<std::type_index, Composition> systems_compositions {};
		std::unordered_map<std::type_index, std::shared_ptr<System>> systems {};

		// ========================================
};

} // namespace ECS

template<typename T>
struct AutomaticRegister {
	private:
		struct ExecRegister {
			ExecRegister() {
				T::onRegister();
			}
		};

		template<ExecRegister&>
		struct ForceReference {};

		static ExecRegister register_object;
		static ForceReference<register_object> referrer;
};

template<typename T> 
typename AutomaticRegister<T>::ExecRegister AutomaticRegister<T>::register_object;

template<class T>
class ComponentRegister : AutomaticRegister<T> {
	public:
		static void onRegister() {
			ECS::Manager::getInstance().RegisterComponent<T>();
		}
};


#endif // DEF_ECS_MANAGER