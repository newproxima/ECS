Basic Entity-Component-System library <!-- omit in toc -->
---

- [**Introduction**](#introduction)
- [**Integration**](#integration)
- [**License**](#license)
- [**Documentation**](#documentation)
	- [Manager](#manager)
	- [Entities](#entities)
	- [Components](#components)
	- [Systems](#systems)

## **Introduction**

This repository contains a tiny ECS library written in C++ that can be used to create video-games and many other applications. 

ECS stands for Entity Component System, the three main parts of this pattern. This pattern allows you to create and manage entities, attach custom components to them and create systems that will be able to automatically interact with components attached to entities.

### Disclaimer <!-- omit in toc -->
This libary is very simple. If you need a fully fledged-out, efficient, library for building serious games/programs with, consider checking out other projects like [EnTT](https://github.com/skypjack/entt) or [EntityX](https://github.com/alecthomas/entityx).

## **Integration**
This library is comprised of a single c++ header file ("_ecs.hpp_") which can be included in source code files:
```cpp
#include "relative/path/to/ecs.hpp"
```

If the file is located oustide your project folder, the folder conatining it must be included when calling the compiler. 

Using [gcc](https://gcc.gnu.org/), add `-Ipath/to/library/directory` to the compiler call. ([see](https://www.rapidtables.com/code/linux/gcc/gcc-i.html))

## **License**
This repository is licensed under the [MIT License]((LICENSE)).

---

## **Documentation**

All library code is contained within the ```ECS``` namespace.

Certain parts of the library can be optimised for either speed or memory usage. To choose between the two you can define the corresponding preprocessor macro using

```cpp
#define ECS_SPEED_OPTIMISATION
```

or

```cpp
#define ECS_MEMORY_OPTIMISATION
```

Their effects however, are relatively small.

### Manager
The whole library relies on the `Manager` class and all actions must pass through it's singleton instance. To retrieve a pointer to the instance, call 
```cpp
Manager* Manager::getInstance()
```

Code examples in this documentation will assume `p_man` is a pointer returned by this method.

### Entities

An entity is a simple `uint16_t` number hiden behind the typedef'd `Entity` type. 

By default there may be up to `UINT16_MAX` (65535) entities existing at once. This value can be modified through the `ECS_MAX_ENTITY_COUNT` macro:

```cpp
#define ECS_MAX_ENTITY_COUNT YOUR_MAX_COUNT
```

##### **Create an entity**<!-- omit in toc -->

```cpp
Entity entity = p_man->CreateEntity();
```

##### **Destroy an entity**<!-- omit in toc -->

```cpp
p_man->DestroyEntity(entity);
```

### Components

A component is any class or struct registered as a component to the Manager. Components can then be attached to and retrieved from entities.

By default, all components can be attached to as many entities as may be created. However this default value can be modified by changing the `COMPONENTS_POOL_DEFAULT_SIZE` constant. Furthermore, this value can be modfied for each component type by calling the `SetMaxComponentCount<T>(const size_t size)` method:
```cpp
p_man->SetMaxComponentCount<MyComponent>(my_size);
```

##### **Registering components**<!-- omit in toc -->

There is two ways of registering a component type:
- Manually, by calling the `RegisterComponent<T>()` with `T` being the type of the component you want to register.
  ```cpp
	struct MyComponent 
	{	... }
	...
	p_man->RegisterComponent<MyComponent>();
	```
- Or automatically, by deriving your component from `ComponentRegister<T>` with `T`, again, being the type of the component itself.
  ```cpp
	struct MyComponent : ComponentRegister<MyComponent> 
	{ ... }
	```

Alternatively, when first being used, any component type will be registered if unknown to the Manager.

##### **Attaching components**<!-- omit in toc -->

Use `AttachComponent<T>(Entity entity, Args... args)` with `T` being the component you want to attach, `entity` the entity you want to attach it to and `args` being any number of arguments that will be passed to the component's constructor.

```cpp
p_man->AttachComponent<MyComponent>(entity, args...);
```

##### **Detaching components**<!-- omit in toc -->

```cpp
p_man->DetachComponent<MyComponent>(entity);
```

Component is destroyed.

##### **Retrieving components**<!-- omit in toc -->

Component must be attached to entity.

```cpp
bool has_component = p_man->hasComponent<MyComponent>(entity);

if (has_component) {
	MyComponent* component = p_man->GetComponent<MyComponent>(entity);
}
```

### Systems

In the ECS pattern, systems are processes which are able to perform actions on every entities which possess the components matching the system's query.

In this library, a system may be an instance of any class which inherits from the `ECS::System` class.

#### **Compositions**<!-- omit in toc -->

Compositions are objects used to represent which components must compose an entity for it to be referenced by a system.
They are defined by:

```cpp
typedef std::bitset<MAX_COMPONENT_TYPES> Composition
```

##### **Define a system class**<!-- omit in toc -->

By deriving any class from the `ECS::System` class will allow it to be registered as system to the manager. The derived class will inherit the `std::set<Entity> composed_entities` property. This property will be automatically updated to hold all entities that match the system's composition requirements.

```cpp
class MySystem : public System {
	{ ... }
};
```


##### **Register system**<!-- omit in toc -->

To register the system you will need to provide the `ECS::Composition` it will query for. 

```cpp
ECS::Composition system_composition = Composition(); // Create composition

p_man->AddToComposition<MyComponent>(system_composition); // Add MyComponent to the composition

std::shared_ptr<MySystem> p_system = p_man->RegisterSystem<MySystem>(system_composition); // Register MySystem and retrieve the std::shared_pointer pointing to it
```

You can also get a pointer to the instance of the system at any point after its registration by using the `GetSystem<S>()` method.


##### **Modify System's composition**<!-- omit in toc -->

TODO